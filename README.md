# README #

# Prerequisites #
* Java 1.7
* Apache Maven 3.2.3
* Apache Tomcat 8.5.47

## End-Points ##

### Create a new search ###
<HOST>/xe/api/search?maxResults=<MAX_RESULTS>

### Return the results in a paging manner ###
<HOST>/xe/api/search/<SEARCH_ID>?startIdx=<PAGE_START_INDEX>&maxResults=<MAX_RESULTS>

### Return a specific ad from the search results ###
<HOST>/xe/api/search/ad/<AD_ID>?searchId=<SEARCH_ID>&sourceId=<SOURCE_ID>

## Compile ##
* Run `mvn clean install`

## Run Application ##
* Copy `/webapp/target/xe.war` to $TOMCAT_HOME/webapps
* Start Tomcat `$TOMCAT_HOME/bin/startup.sh`

## Statistical Data ##
The statistics will be flushed in $TOMCAT_HOME/logs/xe-stats.log upon graceful container shutdown


fa4e4bed-8504-4211-b8c4-9612c35ad8e6