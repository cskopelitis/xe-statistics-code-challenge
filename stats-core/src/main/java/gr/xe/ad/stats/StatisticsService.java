package gr.xe.ad.stats;

import gr.xe.java.codechallenge.Ad;

public interface StatisticsService {

    enum EventType {
        RETURNED, INCLUDED, CLICKED
    }

    /**
     * Triggered when the list of ads is returned
     *
     * @param ads the list of ads returned
     */
    void onAdReturned(Ad[] ads);

    /**
     * Triggered when the list of ads is included in a paginated result
     *
     * @param ads the list of ads returned
     */
    void onAdIncluded(Ad[] ads);

    /**
     * Triggered when an ad is clicked
     *
     * @param sourceId the source of the click event
     * @param ad       the ad that was clicked
     */
    void onAdClicked(String sourceId, Ad ad);
}