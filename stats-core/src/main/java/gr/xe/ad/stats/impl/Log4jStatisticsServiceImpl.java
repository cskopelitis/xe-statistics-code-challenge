package gr.xe.ad.stats.impl;

import gr.xe.ad.stats.StatisticsService;
import gr.xe.java.codechallenge.Ad;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

/**
 * Outputs the raw event data in a file in order to be consumed by a secondary application and aggregated
 */
@Service("log4jStatisticsService")
public class Log4jStatisticsServiceImpl implements StatisticsService {
    private final static Logger LOGGER = Logger.getLogger("ADSTATSLOG");

    @Override
    public void onAdReturned(Ad[] ads) {
        logAds(EventType.RETURNED, ads);
    }

    @Override
    public void onAdIncluded(Ad[] ads) {
        logAds(EventType.INCLUDED, ads);
    }

    @Override
    public void onAdClicked(String sourceId, Ad ad) {
        if (ad == null) {
            LOGGER.warn("No add provided");
            return;
        }
        LOGGER.info(String.format("%s,%s,%s,%s,%s", EventType.CLICKED, ad.getId(), ad.getCustomerId(), ad.getCreatedAt().getTime(), sourceId));
    }

    private void logAds(EventType type, Ad[] ads) {
        for (Ad ad : ads) {
            logAd(type, ad);
        }
    }

    private void logAd(EventType type, Ad ad) {
        LOGGER.info(String.format("%s,%s,%s,%s", type.name(), ad.getId(), ad.getCustomerId(), ad.getCreatedAt().getTime()));
    }
}