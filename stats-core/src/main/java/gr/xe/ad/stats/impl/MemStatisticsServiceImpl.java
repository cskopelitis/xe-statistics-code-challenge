package gr.xe.ad.stats.impl;

import gr.xe.ad.stats.StatisticsService;
import gr.xe.ad.stats.model.AdStats;
import gr.xe.java.codechallenge.Ad;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Stores statistical data on memory
 */
@Service("memStatisticsService")
public class MemStatisticsServiceImpl implements StatisticsService {
    private final static Logger LOGGER = Logger.getLogger("ADSTATSLOG");

    private final Map<String, AdStats> stats = new ConcurrentHashMap<>();

    @Override
    public synchronized void onAdReturned(Ad[] ads) {
        for (Ad ad : ads) {
            AdStats adStats = stats.get(ad.getId());
            if (adStats == null) {
                adStats = new AdStats();
            }
            adStats.returned();
            stats.put(ad.getId(), adStats);
        }
    }

    @Override
    public synchronized void onAdIncluded(Ad[] ads) {
        for (Ad ad : ads) {
            stats.get(ad.getId()).included();
        }
    }

    @Override
    public synchronized void onAdClicked(String sourceId, Ad ad) {
        if (ad == null) {
            LOGGER.warn("No add provided");
            return;
        }
        stats.get(ad.getId()).clicked(sourceId);
    }

    @PreDestroy
    public void persist() {
        for (Map.Entry<String, AdStats> adIdWithStats : stats.entrySet()) {
            String adId = adIdWithStats.getKey();
            AdStats adStats = adIdWithStats.getValue();
            LOGGER.info(String.format("%s,%s,%s", adId, EventType.RETURNED.name(), adStats.getReturns()));
            LOGGER.info(String.format("%s,%s,%s", adId, EventType.INCLUDED.name(), adStats.getIncludes()));
            LOGGER.info(String.format("%s,%s,%s,ALL", adId, EventType.CLICKED.name(), adStats.getTotalClicks()));
            for (Map.Entry<String, Integer> sourceClicks : adStats.getClicks().entrySet()) {
                LOGGER.info(String.format("%s,%s,%s,%s", adId, EventType.CLICKED.name(), sourceClicks.getValue(), sourceClicks.getKey()));
            }
        }
    }

    Map<String, AdStats> getStats() {
        return stats;
    }
}