package gr.xe.ad.stats.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public final class AdStats implements Serializable {
    private static final long serialVersionUID = 8858299057928249572L;

    private int returns;
    private int includes;
    private final Map<String, Integer> clicks = new HashMap<>();

    public void returned() {
        returns++;
    }

    public void included() {
        includes++;
    }

    public void clicked(String sourceId) {
        Integer clicksFromSource = clicks.get(sourceId);
        if (clicksFromSource == null) {
            clicksFromSource = 0;
        }
        clicks.put(sourceId, ++clicksFromSource);
    }

    public int getReturns() {
        return returns;
    }

    public int getIncludes() {
        return includes;
    }

    public Map<String, Integer> getClicks() {
        return clicks;
    }

    public int getTotalClicks() {
        int totalClicks = 0;
        for (Integer sourceClicks : clicks.values()) {
            totalClicks += sourceClicks;
        }
        return totalClicks;
    }
}