package gr.xe.ad.stats.impl;

import gr.xe.ad.stats.model.AdStats;
import gr.xe.java.codechallenge.Ad;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

public class MemStatisticsServiceImplTest {

    private final static int THREADS = 500;
    private final static int NO_OF_ADS = 10;

    private final static String SOURCE_1 = "src_1";
    private final static String SOURCE_2 = "src_2";

    private MemStatisticsServiceImpl memStatisticsService;

    private Ad[] ads;

    @Before
    public void setUp() throws Exception {
        memStatisticsService = new MemStatisticsServiceImpl();

        ads = new Ad[NO_OF_ADS];
        for (int i = 0; i < NO_OF_ADS; i++) {
            ads[i] = new Ad();
        }
    }

    @Test
    public void testOnAdReturned() {
        memStatisticsService.onAdReturned(ads);

        for (Ad ad : ads) {
            AdStats adStats = memStatisticsService.getStats().get(ad.getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(0, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }
    }

    @Test
    public void testOnAdIncluded() {
        memStatisticsService.onAdReturned(ads);

        memStatisticsService.onAdIncluded(Arrays.copyOfRange(ads, 0, 2));

        for (int i = 0; i < 2; i++) {
            AdStats adStats = memStatisticsService.getStats().get(ads[i].getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(1, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }

        for (int i = 2; i < ads.length; i++) {
            AdStats adStats = memStatisticsService.getStats().get(ads[i].getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(0, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }
    }

    @Test
    public void testOnAdIncludedMultiThreaded() throws InterruptedException {
        memStatisticsService.onAdReturned(ads);

        CountDownLatch startLatch = new CountDownLatch(THREADS);
        CountDownLatch finishLatch = new CountDownLatch(THREADS);

        Set<Worker> workers = new HashSet<>();
        for (int i = 0; i < THREADS; i++) {
            Worker w = new IncludeWorker(ads, startLatch, finishLatch, "IN-WORKER-" + i);
            workers.add(w);
        }

        for (Worker worker : workers) {
            worker.start();
        }

        // wait for all threads to finish
        finishLatch.await();

        for (Ad ad : ads) {
            AdStats adStats = memStatisticsService.getStats().get(ad.getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(THREADS, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }
    }

    @Test
    public void testOnAdClicked() {
        memStatisticsService.onAdReturned(ads);

        memStatisticsService.onAdClicked(SOURCE_1, ads[NO_OF_ADS - 1]);
        memStatisticsService.onAdClicked(SOURCE_2, ads[NO_OF_ADS - 1]);
        memStatisticsService.onAdClicked(SOURCE_2, ads[NO_OF_ADS - 2]);

        for (int i = 0; i < NO_OF_ADS - 2; i++) {
            AdStats adStats = memStatisticsService.getStats().get(ads[i].getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(0, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }

        AdStats adStats1 = memStatisticsService.getStats().get(ads[NO_OF_ADS - 1].getId());
        Assert.assertEquals(2, adStats1.getTotalClicks());
        Assert.assertEquals(Integer.valueOf(1), adStats1.getClicks().get(SOURCE_1));
        Assert.assertEquals(Integer.valueOf(1), adStats1.getClicks().get(SOURCE_2));

        AdStats adStats2 = memStatisticsService.getStats().get(ads[NO_OF_ADS - 2].getId());
        Assert.assertEquals(1, adStats2.getTotalClicks());
        Assert.assertNull(adStats2.getClicks().get(SOURCE_1));
        Assert.assertEquals(Integer.valueOf(1), adStats2.getClicks().get(SOURCE_2));
    }

    @Test
    public void testOnAdClickedMultiThreaded() throws InterruptedException {
        memStatisticsService.onAdReturned(ads);

        CountDownLatch startLatch = new CountDownLatch(THREADS);
        CountDownLatch finishLatch = new CountDownLatch(THREADS);

        Set<Worker> workers = new HashSet<>();
        for (int i = 0; i < THREADS; i++) {
            Worker w = new ClickWorker(ads[0], startLatch, finishLatch, "IN-WORKER-" + i);
            workers.add(w);
        }

        for (Worker worker : workers) {
            worker.start();
        }

        // wait for all threads to finish
        finishLatch.await();

        for (int i = 1; i < NO_OF_ADS; i++) {
            AdStats adStats = memStatisticsService.getStats().get(ads[i].getId());
            Assert.assertEquals(1, adStats.getReturns());
            Assert.assertEquals(0, adStats.getIncludes());
            Assert.assertEquals(0, adStats.getTotalClicks());
        }

        AdStats adStats = memStatisticsService.getStats().get(ads[0].getId());
        Assert.assertEquals(1, adStats.getReturns());
        Assert.assertEquals(0, adStats.getIncludes());
        Assert.assertEquals(THREADS, adStats.getTotalClicks());
    }

    abstract class Worker extends Thread {

        private CountDownLatch latch;
        private CountDownLatch finishLatch;

        private final Random random = new Random();

        public Worker(CountDownLatch latch, CountDownLatch finishLatch, String workerName) {
            super(workerName);
            this.latch = latch;
            this.finishLatch = finishLatch;
        }

        public abstract void doWork();

        @Override
        public void run() {
            try {
                try {
                    Thread.sleep(Math.abs(random.nextLong()) % 3000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                latch.countDown();
                // start all threads at the same time
                latch.await();

                doWork();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                finishLatch.countDown();
            }
        }
    }

    class IncludeWorker extends Worker {

        protected Ad[] workerAds;

        public IncludeWorker(Ad[] workerAds, CountDownLatch latch, CountDownLatch finishLatch, String workerName) {
            super(latch, finishLatch, workerName);
            this.workerAds = workerAds;
        }

        @Override
        public void doWork() {
            memStatisticsService.onAdIncluded(workerAds);
        }
    }

    class ClickWorker extends Worker {

        private Ad workerAd;

        public ClickWorker(Ad workerAd, CountDownLatch latch, CountDownLatch finishLatch, String workerName) {
            super(latch, finishLatch, workerName);
            this.workerAd = workerAd;
        }

        @Override
        public void doWork() {
            memStatisticsService.onAdClicked(SOURCE_1, workerAd);
        }
    }
}