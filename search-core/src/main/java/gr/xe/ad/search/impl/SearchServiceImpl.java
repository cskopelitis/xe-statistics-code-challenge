package gr.xe.ad.search.impl;

import gr.xe.ad.search.SearchService;
import gr.xe.java.codechallenge.Ad;
import gr.xe.java.codechallenge.Search;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.TreeMap;

@Service("searchService")
public class SearchServiceImpl implements SearchService {

    private final Map<String, Search> cachedResults = new TreeMap<>();

    public Search search() {
        Search search = new Search();
        cachedResults.put(search.getSearchId(), search);
        return search;
    }

    public Search search(String searchId) {
        return cachedResults.get(searchId);
    }

    @Override
    public Ad searchAd(String adId) {
        for (Search search : cachedResults.values()) {
            for (Ad ad : search.getSearchResults()) {
                if (adId.equals(ad.getId())) {
                    return ad;
                }
            }
        }
        return null;
    }

    public Ad search(String searchId, String adId) {
        Search cachedSearch = cachedResults.get(searchId);
        if (cachedSearch == null) {
            throw new IllegalArgumentException(String.format("search with id %s does not exist", searchId));
        }

        for (Ad ad : cachedSearch.getSearchResults()) {
            if (adId.equals(ad.getId())) {
                return ad;
            }
        }
        return null;
    }
}