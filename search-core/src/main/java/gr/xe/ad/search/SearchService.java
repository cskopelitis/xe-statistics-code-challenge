package gr.xe.ad.search;

import gr.xe.java.codechallenge.Ad;
import gr.xe.java.codechallenge.Search;

public interface SearchService {

    /**
     * Creates a new search based on the provided search parameters (omitted because this is a mock). The results of the
     * search are cached on memory
     *
     * @return an object containing the search results
     */
    Search search();

    /**
     * @param searchId the search id
     * @return Returns a cached search or null if the search with the specified ID does not exist
     */
    Search search(String searchId);

    /**
     * @param adId the Ad ID
     * @return Returns a specific ad corresponding to the provided ID, or null if ad is not found
     */
    Ad searchAd(String adId);

    /**
     * @param searchId the search id
     * @param adId     the unique Ad id
     * @return the specified Ad corresponding to the provided search ID and Ad ID
     */
    Ad search(String searchId, String adId);
}