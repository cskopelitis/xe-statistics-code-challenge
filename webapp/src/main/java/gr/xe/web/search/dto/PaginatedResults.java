package gr.xe.web.search.dto;

import gr.xe.java.codechallenge.Ad;

import java.io.Serializable;
import java.util.Arrays;

public class PaginatedResults implements Serializable {
    private static final long serialVersionUID = 8229914095854035947L;

    private String searchId;
    private Ad[] results;

    public PaginatedResults() {
    }

    public PaginatedResults(String searchId) {
        this.searchId = searchId;
    }

    public void addResults(int startIdx, int pageSize, final Ad[] ads) {
        int rangeTo = startIdx + pageSize;
        if (startIdx + pageSize > ads.length) {
            rangeTo = ads.length;
        }
        results = Arrays.copyOfRange(ads, startIdx, rangeTo);
    }

    public String getSearchId() {
        return searchId;
    }

    public void setSearchId(String searchId) {
        this.searchId = searchId;
    }

    public Ad[] getResults() {
        return results;
    }

    public void setResults(Ad[] results) {
        this.results = results;
    }
}