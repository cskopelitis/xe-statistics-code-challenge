package gr.xe.web.search;

import gr.xe.ad.search.SearchService;
import gr.xe.ad.stats.StatisticsService;
import gr.xe.java.codechallenge.Ad;
import gr.xe.java.codechallenge.Search;
import gr.xe.web.search.dto.PaginatedResults;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class SearchController {
    private static final Logger LOGGER = Logger.getLogger(SearchController.class);

    @Autowired
    @Qualifier("memStatisticsService")
    private StatisticsService statisticsService;
    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/heartbeat", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String heartbeat() {
        LOGGER.debug("[GET] /api/heartbeat");
        return "OK";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaginatedResults search(@RequestParam int maxResults) {
        LOGGER.debug(String.format("[POST] /api/search?maxResults=%s", maxResults));

        Search search = searchService.search();
        statisticsService.onAdReturned(search.getSearchResults());

        PaginatedResults paginatedResults = new PaginatedResults(search.getSearchId());

        paginatedResults.addResults(0, maxResults, search.getSearchResults());
        statisticsService.onAdIncluded(paginatedResults.getResults());

        return paginatedResults;
    }

    @RequestMapping(value = "/search/{searchId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaginatedResults search(@PathVariable String searchId, @RequestParam int startIdx, @RequestParam int maxResults) {
        LOGGER.debug(String.format("[POST] /api/search/%s?startIdx=%s&maxResults=%s", searchId, startIdx, maxResults));

        Search search = searchService.search(searchId);

        PaginatedResults paginatedResults = new PaginatedResults(search.getSearchId());

        paginatedResults.addResults(startIdx, maxResults, search.getSearchResults());
        statisticsService.onAdIncluded(paginatedResults.getResults());

        return paginatedResults;
    }

    @RequestMapping(value = "/search/ad/{adId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Ad search(@RequestParam(required = false) String searchId, @PathVariable String adId, @RequestParam String sourceId) {
        LOGGER.debug(String.format("[GET] /api/search/ad/%s?searchId=%s&sourceId=%s", adId, searchId, sourceId));

        Ad ad;
        if (searchId != null) {
            ad = searchService.search(searchId, adId);
        } else {
            ad = searchService.searchAd(adId);
        }
        statisticsService.onAdClicked(sourceId, ad);

        return ad;
    }
}